package com.mycompany.l03.capabilities;

import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities
 */
public class CapabilitiesTests {

    private WebDriver driver;

    @Test
    public void firefox() {
        var capabilities = BrowserCapabilities.getCapabilities(Browsers.FIREFOX);
        driver = new FirefoxDriver((FirefoxOptions) capabilities);
        System.out.println(((HasCapabilities) driver).getCapabilities());
    }

    @Test
    public void chrome()  {
        var capabilities = BrowserCapabilities.getCapabilities(Browsers.CHROME);
        driver = new ChromeDriver((ChromeOptions) capabilities);
        System.out.println(((HasCapabilities) driver).getCapabilities());
    }

    @AfterMethod
    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }
}
