package com.mycompany.l03.capabilities;

public enum Browsers {
    FIREFOX("firefox"),
    CHROME("chrome"),
    OPERA("opera");

    private String browser;

    Browsers(String browser) {
        this.browser = browser;
    }

    @Override
    public String toString() {
        return browser;
    }
}
